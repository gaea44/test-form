<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Opportunity;
use AppBundle\Form\OpportunityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class OpportunityController
 * @package AppBundle\Controller
 */
class OpportunityController extends Controller
{
    /**
     * @Route("/opportunities/new")
     *
     * @param Request $request
     *
     * @return Response
     * @throws \Exception
     */
    public function newAction(Request $request)
    {
        $opportunity = new Opportunity();

        return $this->processForm($opportunity, $request);
    }

    /**
     * @Route("/opportunities/{id}", name="opportunity_edit")
     *
     * @param int     $id
     * @param Request $request
     *
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function editAction($id, Request $request)
    {
        $opportunity = $this->getDoctrine()->getRepository('AppBundle:Opportunity')
            ->find($id);

        if (!$opportunity) {
            $this->createNotFoundException();
        }

        return $this->processForm($opportunity, $request);
    }

    /**
     * @param Opportunity $opportunity
     * @param Request     $request
     *
     * @return Response
     * @throws \Exception
     */
    private function processForm(Opportunity $opportunity, Request $request)
    {
        $form = $this->createForm(OpportunityType::class, $opportunity);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if (!$form->isValid()) {
                $errors = $this->getErrorsFromForm($form);

                throw new \Exception(json_encode($errors));
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($opportunity);
            $em->flush();

            $url = $this->generateUrl('opportunity_edit', [
                'id' => $opportunity->getId(),
            ]);

            return $this->redirect($url);
        }

        return $this->render('opportunity/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param FormInterface $form
     *
     * @return array
     */
    private function getErrorsFromForm(FormInterface $form)
    {
        $errors = [];
        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }

        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = $this->getErrorsFromForm($childForm)) {
                    $errors[$childForm->getName()] = $childErrors;
                }
            }
        }

        return $errors;
    }
}
