test-form
=========

Just testing symfony forms

1. clone files:

    $ git@gitlab.com:gaea44/test-form.git

2. install dependencies

    $ composer install

3. run local server

    $ bin/console server:run

4. install fixtures

    $ bin/console hautelook_alice:fixtures:load

5. go to http://localhost:8000/opportunities/new and submit the form
